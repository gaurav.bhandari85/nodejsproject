


import svgMap from 'svgmap';
import 'svgmap/dist/svgMap.min.css';

const express = require('express');
const app = express();
// If you want to use YAML files
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./swaggerDocs/swagger.yaml');
// Ifwant to use JSON files
//const swaggerDocument = require('./swagger.json');

const swaggerUi = require('swagger-ui-express');

//app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
var options = {}

// default URL for website
app.use('/', function(req,res){
    res.sendFile(path.join(__dirname+'/express/index.html'));
    //__dirname : It will resolve to your project folder.
  });

app.use('/api-docs', function(req, res, next){
    swaggerDocument.host = req.get('host');
    req.swaggerDoc = swaggerDocument;
    next();
}, swaggerUi.serveFiles(swaggerDocument, options), swaggerUi.setup());

app.listen(3000);